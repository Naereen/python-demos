#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of looping over lists of strings/caracters.
Demo of one iterator tool from the itertools module (the product function, for cartesian product).

@date: Mon Feb 9 22:38:00 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

# Import one nice function
from itertools import product


# Alphabet 'a' to 'z', of size 26
alpha = [ chr(i) for i in xrange(97, 123) ]
print "Working with the alphabat", alpha, "of size", len(alpha)


def crack(password='abc', verb=False):
    """ Try one by one every words of the size of the password, from 'aa..a' to 'zz..z'.
    Returns the number of useless tries before finding the password, or fail by raising one Exception.
    """
    # Be sure to only work with lower case
    password = password.lower()

    if not password.isalpha():
        raise ValueError("crack: the password is not an alphabetic string!")
        # we only try passwords that are in [a-z]*

    n = len(password)
    total = len(alpha)**n

    print "\nWe try to find the password", password, "we start a huuuuge for loop of length", total

    count = 0  # number of tries

    # The main for loop is equivalent to n nested loops like this
    # for i in alpha:
        # for j in alpha:
        #     for k in alpha:
        #         ...
        #             for m in alpha:

    # If the size n is fixed, it can also be written like this
    # for i, j, k, l, m in product(alpha, alpha, alpha, alpha, alpha):

    # But the general way to do it is:
    #  1. by defining the tuple (alpha,)*n, ie the tuple (alpha, alpha, .., alpha) of size n
    bigtuple = (alpha,)*n

    #  2. and then give it as a list of arguments to the product function
    #     with the f(*tuple) syntax
    for x in product(*bigtuple):  # Big loop of size total 26**n
        # x = ('a', 'a', .., 'a') then ('a', 'a', .., 'b') then .. then ('z', 'z', .., 'z')

        # guess is like x[0] + "" + x[1] + "" + .. + "" + x[n-1]
        guess = "".join(x)

        if verb:
            print "The current value for the tuple x is:", x, "and this is the", count, "th guess."
            print "Trying to compare the password", password, "with the guess", guess

        count += 1

        if guess == password:
            print "We guessed the password, and we have tried", float(count)/float(total), "% of all the possible guesses... Yeepee!"
            return count
            # We are done !

    # If we are out of the for loop without having return any count, that is a failure!
    raise ValueError("crack: the password has not been found!")


# %% First demo
password1 = 'abba'
count = crack(password1, verb=True)
print "For the first password", password1, "it took", count, "number of tries."


# %% Longer demo. It needs 146466273 tries before finding the password.
# On my computer, it took 1min 1s
password2 = 'aminice'
# count = crack(password2)
print "For the second password", password2, "it took", count, "number of tries."

# You can measure the execution time with this line
# %timeit count = crack(password2)  # ONLY in IPython !
# 1 loops, best of 3: 1min 1s per loop


# %% Small demo that fails, the password is not alphabetic
password3 = u'été'
try:
    count = crack(password3)
    print "For the third password", password3, "it took", count, "number of tries."
except:
    print "For the third password", password3, "it failed!"


# End
from sys import argv
print "\n\nThe example", argv[0], "is done."
print "More are available online at https://bitbucket.org/lbesson/python-demos/ !"
