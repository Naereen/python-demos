#! /usr/bin/env python
# -*- coding: utf-8 -*-
r"""
Demo of the sympy Python toolbox, for computing formal and numerical integrals of two a dimensional function.

Remark: this example $\int_0^1 \int_y^1 cos(x**2) dx dy$ comes from the first MA102 quiz done the 29-01-2015.

@date: Thu Jan 29 11:00:44 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

# %% Starting by importing sympy (SymPy)
from sympy import *
# from sympyprinting import *
# FIXME: I want to print Integral(f(x, y), (x, y, 1), (y, 0, 1)) with UTF in the console

# Clean way to create variables x, y
from sympy.abc import x, y
# x, y = var('x'), var('y')

# %% Define the function f
def f(x, y):
    """ Example function, f(x, y) = cos(x**2). """
    return cos(x**2)

print "For that function f(x, y) = cos(x**2):"
print "We want compute the double integral on the domain D."
print "\tD = {(x, y) : 0 < x < 1, 0 < y < 1, 0 < x + y < 1}."

# %% Compute the double-integral, first on x, then on y
print "\nFirst integral is x, then y: $\int_0^1 \int_y^1 f(x, y) dx dy$"

# Formal input
sxy = Integral(f(x, y), (x, y, 1), (y, 0, 1))
print "SymPy represents formally this as:", sxy, "\nAnd it can be sexy:"
pprint(sxy)

# Formal computation: let integrate it!
print "But SymPy can also compute it (formally again)."
intf_x_y = integrate(integrate(f(x, y), (x, y, 1)), (y, 0, 1))
print "But that first integral is ugly!\n", intf_x_y

try:
    print "But we can approximate its value: {:g}.".format(float(intf_x_y))
    print "(More precisely: {:.20g})".format(float(intf_x_y))
except:
    print "And it is too ulgy to compute its approximate value!"

# %% Compute the double-integral, first on y, then on x
print "\nSecond integral is y, then x: $\int_0^1 \int_0^x f(x, y) dy dx$"

# Formal input
syx = Integral(f(x, y), (y, 0, x), (x, 0, 1))
print "SymPy represents formally this as:"
pprint(syx)

# Formal computation: let integrate it!
print "But SymPy can also compute it (formally again)."
intf_y_x = integrate(integrate(f(x, y), (y, 0, x)), (x, 0, 1))

print "That second integral is simpler:\n", pretty(intf_y_x)
print "This is a nice formal result (which was expected for the Quiz)."

try:
    print "But we can approximate its value: {:g}.".format(float(intf_y_x))
    print "(More precisely: {:.20g})".format(float(intf_y_x))
except:
    print "And it is too ulgy to compute its approximate value!"


# %% One 3D plot for this function f
# We import the pylab environment.
# It is like calling %pylab in IPython
from pylab import *
fig = figure(1)

from mpl_toolkits.mplot3d import Axes3D
ax = Axes3D(fig)
n = 16
X = linspace(0, 1, n)
Y = linspace(0, 1, n)
X, Y = meshgrid(X, Y)
Z = cos(X**2)

ax.set_zlim(0, 1)
title(r'Plot of the surface $f(x, y) = cos(x^2)$ on $[0, 1]\times[0,1]$')

ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='jet')
ax.contourf(X, Y, Z, 10, zdir='z', offset=0, cmap='jet', alpha=.75)

#C = contour(X, Y, Z, 10, colors='black', linewidth=.5)
#clabel(C, inline=1, fontsize=10)
#xticks([]), yticks([])

savefig('DemoSympy__figure1.svg')
