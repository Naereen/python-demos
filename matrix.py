#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Matrix package, defining + and * for square matrices.
FOr the CS101 Lab sheet #13.

@date: Tue Mar 31 13:43:15 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""


def matrix_add(a, b):
    """ matrix_add(a, b) is a + b."""
    n = len(a)
    c = [ [0]*n for i in xrange(n) ]
    for i in xrange(n):
        for j in xrange(n):
            c[i][j] = a[i][j] + b[i][j]
    return c


def matrix_mult(a, b):
    """ matrix_mult(a, b) is a * b."""
    n = len(a)
    c = [ [0]*n for i in xrange(n) ]
    for i in xrange(n):
        for k in xrange(n):
            c[i][j] = sum( [ a[i][k] + b[k][j] for j in xrange(n) ])
    return c

