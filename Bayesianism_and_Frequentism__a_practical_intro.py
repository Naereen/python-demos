#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Code from "Bayesianism and Frequentism : a practical intro".


Reference: http://jakevdp.github.io/blog/2014/03/11/frequentism-and-bayesianism-a-practical-intro/.

@date: Wed Apr 15 10:01:48 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

# %% First, Generating some simple photon count data
import numpy as np
from scipy import stats

np.random.seed(1)  # for repeatability

# True flux, say number of photons measured in 1 second
F_true = 1000

# number of measurements
N = 50

# N measurements of the flux
F = stats.poisson(F_true).rvs(N)

# errors on Poisson counts estimated via square root
e = np.sqrt(F)


# %% Now let's make a simple visualization of the "measured" data:
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
ax.set_xlabel("Flux")
ax.set_ylabel("Measurement number");
plt.title("Simple visualization of the some simple photon count data.")

# True flux
ax.vlines([F_true], 0, N, linewidth=8, alpha=0.6)
# Error bar (for each measurement)
ax.errorbar(F, np.arange(N), xerr=e, fmt='ok', ecolor='gray', alpha=0.6)

# Important question
print "Given our measurements and errors, what is our best estimate of the true flux?"


# %% Frequentist Approach to Simple Photon Counts
print "Frequentist Approach to Simple Photon Counts:"

w = 1. / e ** 2
print(""" 1. F_true = {0}
 2. F_est  = {1:.0f} +/- {2:.0f} (based on {3} measurements)""".format(F_true, (w * F).sum() / w.sum(), w.sum() ** -0.5, N))


# %% Bayesian Approach to Simple Photon Counts
print "Bayesian Approach to Simple Photon Counts:"



def log_prior(theta):
    return 1  # flat prior

def log_likelihood(theta, F, e):
    return -0.5 * np.sum(np.log(2 * np.pi * e ** 2)
                         + (F - theta[0]) ** 2 / e ** 2)

def log_posterior(theta, F, e):
    return log_prior(theta) + log_likelihood(theta, F, e)



