#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Example of Python programs to solve the problems of the First Mid Term Examination for CS101.

@date: Fri Feb 20 16:26:03 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

# %% Problem I : type 1
# Find the sum of all the positive integer numbers (n > 0) that are divisible by 7 or 11 and that are below 1 lack (ie. n < 100000).
# For example, for the numbers below 50, we have 7, 11, 14, 21, 22, 28, 33, 35, 42, 44 and 49 so the sum is 7 + 11 + 14 + 21 + 22 + 28 + 33 + 35 + 42 + 44 + 49 = 306.

def sumOfAllMultiplesOf7or11(N=50):
    currentSum = 0

    for n in xrange(1, N):
        if (n % 7 == 0) or (n % 11 == 0):
            currentSum += n

    print "Problem 1 (1st kind) ==> The sum of all the positive integer numbers that are multiple of 7 or 11 and smaller than", N, "is", currentSum


sumOfAllMultiplesOf7or11(50)
sumOfAllMultiplesOf7or11(10000)


# %% Problem I : type 2
# Find the product of all the positive integer numbers (n > 0) that are divisible by 3 or 5 and that are below 30 (ie. n < 30).
# For example, for the numbers below 15, we have 3, 5, 6, 9, 10 and 12 so the product is 3 * 5 * 6 * 9 * 10 * 12 = 97200.

def productOfAllMultiplesOf3or5(N=15):
    currentProduct = 1

    for n in xrange(1, N):
        if (n % 3 == 0) or (n % 5 == 0):
            currentProduct *= n

    print "Problem 1 (2nd kind) ==> The product of all the positive integer numbers that are multiple of 3 or 5 and smaller than", N, "is", currentProduct


productOfAllMultiplesOf3or5(15)
productOfAllMultiplesOf3or5(30)


# %% Problem II : type 1
# Find the sum of the first 30 Fibonacci numbers F_0 + F_1 + ... + F_28 + F_29.
# We remind that the sequence F_n is given by F_0 = 0, F_1 = 1, and F_n+2 = F_n+1 + F_n.
# For example, the first 5 Fibonacci numbers is 1, 2, 3, 5 and 8 so, their sum is 1 + 2 + 3 + 5 + 8 = 19.

def sumFirstFibonacciNumbers(k=5):
    currentSum = 0
    fibs = [0, 1]
    n = 0
    while n < k:
        fibs.append(fibs[n] + fibs[n+1])
        currentSum += fibs[n]
        print "We added F_n =", fibs[n], "( for n =", n, ") to the current sum, which is now", currentSum
        n += 1

    print "Problem 2 (1st kind) ==> the sum of the first", k, "Fibonacci numbers is", currentSum


sumFirstFibonacciNumbers(5)
sumFirstFibonacciNumbers(30)


# %% Problem II : type 2
# Give the first 14 values of the factorial numbers (0!, 1!, . . . , 13!),
# You also need to find from which value of n its factorial n! is strictly greater than 10**15.
# We remind that the factorial function is defined with 0! = 1, n! = 1 * 2 * ... * n
# (and satisfies (n+1)! = (n + 1) * n! recursively).

def printFirstFactorial(k):
    n = 0
    facts = [1]  # 0! = 1
    while n < k:
        facts.append(facts[n] * (n+1))
        print "For n =", n, "its factorial n! is", facts[n]
        n += 1

    print "Done, I printed the first", k, "values of factorial n ( for n from 0 to", k-1, ")."


print "Problem 2 (2nd kind) ==> "
printFirstFactorial(14)


def smallestFactorialBiggerThan(x):
    n = 0
    facts = [1]
    while facts[n] <= x:
        facts.append(facts[n] * (n+1))
        n += 1

    print "For n =", n, "its factorial is", facts[n]
    return n


n = smallestFactorialBiggerThan(1e15)
print "Problem 2 (2nd kind) ==> the smallest n such that n! > 10**15 is", n
