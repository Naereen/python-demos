#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Examples of computation of eigen values and eigen vectors with SymPy (Symbolic Python).

The examples are coming from MA102 Tutorial Sheet 9 (given the 06-04 10-04 week).


@date: Tue Apr 07 21:29:59 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

from sympy.abc import *
from sympy import *
init_printing(use_unicode=False, wrap_line=False, no_global=True)

from sympy.matrices import *

def print_eigen(C):
    eigen_vals = C.eigenvals()
    print "Its eigen values are:"
    print(eigen_vals)
    eigen_vects = C.eigenvects()
    print "Its eigen vectors are:"
    print(eigen_vects)

def problem(A, B):
    print "\nFor the matrix A:"
    print(A)
    print_eigen(A)

    print "\nFor the matrix B:"
    print(B)
    print_eigen(B)


# %% Problem 1
A = Matrix( [[1, 4], [2, 3]] )
B = Matrix( [[2, 4], [2, 4]] )

problem(A, B)


# %% Problem 2
A = Matrix( [[0, 2], [1, 1]] )
B = A.inv()

print "A and A inverse:", A, B
problem(A, B)


# %% Problem 3
A = Matrix( [[-1, 3], [2, 0]] )
B = A ** 2

print "A and A square:", A, B
problem(A, B)


# %% Problem 4
A = Matrix( [[3, 0], [1, 1]] )
B = Matrix( [[1, 1], [0, 3]] )

problem(A, B)

C = A + B
print "A + B is:", C
print_eigen(C)


# %% Problem 5
A = Matrix( [[1, 0], [1, 1]] )
B = Matrix( [[1, 2], [0, 1]] )

problem(A, B)

print "A * B is:", A * B
print_eigen(A * B)

print "B * A is:", B * A
print_eigen(B * A)
