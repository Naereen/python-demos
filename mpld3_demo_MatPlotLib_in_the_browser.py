#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of mpld3 module, which brings MatPlotLib plotting to the browser.

Reference is http://mpld3.github.io/quickstart.html.

@date: Tue Mar 24 16:19:05 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
import mpld3

# %% First example
plt.plot([3,1,4,1,5], 'ks-', mec='w', mew=5, ms=20)

# This function is mpld3's equivalent of matplotlib's pyplot.show function.
# It will convert the current figure to html using fig_to_d3(),
# start a local webserver which serves this html,
# and (if the operating system allows it) automatically open this page in the web browser.
mpld3.show()

# %% Second example (https://mdboom.github.io/blog/2012/10/11/matplotlib-in-the-browser-its-coming/)
# https://github.com/mdboom/mpl_browser_experiments

n = 50
x = np.linspace(-1.5,1.5,n)
y = np.linspace(-1.5,1.5,n*2)
X,Y = np.meshgrid(x,y);
Qx = np.cos(Y) - np.cos(X)
Qz = np.sin(Y) + np.sin(X)
Qx = (Qx + 1.1)
Z = np.sqrt(X**2 + Y**2)/5;
Z = (Z - Z.min()) / (Z.max() - Z.min())

# The color array can include masked values:
Zm = ma.masked_where(np.fabs(Qz) < 0.5*np.amax(Qz), Z)

fig = plt.figure()
ax = fig.add_subplot(121)
ax.set_axis_bgcolor("#bdb76b")
ax.pcolormesh(Qx,Qz,Z, shading='gouraud')
ax.set_title('Without masked values')

ax = fig.add_subplot(122)
ax.set_axis_bgcolor("#bdb76b")
col = ax.pcolormesh(Qx,Qz,Zm,shading='gouraud')
ax.set_title('With masked values')

mpld3.show()