#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generating a random family of integer vectors of a certain rank with Python using the NumPy module.

Based on numpy.random.random_integers, which documentation is on http://docs.scipy.org/doc/numpy/reference/generated/numpy.random.random_integers.html#numpy.random.random_integers

@date: Thu Feb 26 12:21:36 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

import numpy
from numpy.linalg import matrix_rank

def generate_random_matrix_of_rank(n, m, rank, a=-10, b=10):
    """ Produces a matrix of size n, m (ie m vectors of size n, 1) of rank equal to rank."""
    assert n >= 0
    assert m >= 0
    assert rank >= 0
    rank = min([rank, n, m])
    steps = 1
    M = numpy.random.random_integers(a, b, (n, m))
    # BAD way to do it (might take too much time)
    while matrix_rank(M) != rank:
        M = numpy.random.random_integers(a, b, (n, m))
        steps += 1
    print "Done with", steps, "steps."
    return M


for group in [ 'A', 'B' ]:
    for kind in ['1', '2']:
        print "For the paper for group", group, "of kind", kind
        M = generate_random_matrix_of_rank(3, 3, 2)
        print " ==> a random family of size 3 vectors of size (3, 1), of spanned space of dimension 2 :\n", M
