#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of reading and displaying a BMP color image (bitmap).

Reference for the file format is: https://en.wikipedia.org/wiki/BMP_file_format.

@date: Mon Apr 13 11:40:55 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import struct

def rgb_to_gray(rgb):
    """ Convert a (r, g, b) tuple color to a gray-scale integer (between 0 and 255)."""
    return int(0.2126*rgb[0] + 0.7152*rgb[1] + 0.0722*rgb[2])


# %% Read and plot the image with MatPlotLib
if __name__ == '__main__':
    path = "lena_c.bmp"
#    path = "marbles.bmp"
    from matplotlib.pyplot import imshow, imread
    import matplotlib.pyplot as plt

    myimage = imread(path)  # , as_grey=True)
    width, height, bits_per_pixel = myimage.shape

    plt.figure()
    plt.title("Colored Lena \n(read with MatPlotLib: imread('lena_c.bmp'))")
    imshow(myimage, cmap=None)
    plt.savefig("lena_c_matplotlib_pyplot_imread.png")

    plt.figure()
    # FIXME: This step is long !
    myimage_gray = [[rgb_to_gray(tuple(myimage[i, j, :])) for j in xrange(height)] for i in xrange(width)]
    plt.title("Gray-scale Lena \n(read with MatPlotLib: imread('lena_c.bmp'))")
    imshow(myimage_gray, cmap='gray')
    plt.savefig("lena_c_gray_matplotlib_pyplot_imread.png")


# %% Do it manually

def bmp_to_matrix(path="lena_c.bmp"):
    """ Trying to convert the BMP image given by its path, into a matrix of gray-scale values (0 to 255)."""

    # Read the "lena.bmp" image in binary mode
    lena = open(path, 'rb')
    content = list(lena)


    # %% Reading the header
    header = content[0][:-1]
#    print "The image header has size", len(header), "bytes."
    # Check that it is a good BMP file
    if str(header[0:2]) not in ['BM', 'BA', 'CI', 'CP', 'IC', 'PT']:
        raise ValueError("bmp_to_matrix(): file ('{}') seems to not be a good BMP file.".format(path))

    # Reference is  https://en.wikipedia.org/wiki/BMP_file_format#Bitmap_file_header

    # Checking that the file size is correct
    size_bytes, = struct.unpack('<i', header[2:6])
    if size_bytes != sum([len(i) for i in content]):
        print "ERROR: the file seems to be corrupted (wrong size in header[2:6]."
        raise ValueError("bmp_to_matrix(): file ('{}') seems to be corrupted (wrong size in header[2:6].".format(path))
    else:
        print "The BMP image is stored with", size_bytes, "bytes, which is about", size_bytes / 1024, "kB."

    # Finding the offset
    offset, = struct.unpack('<i', header[10:14])
    print "The bitmap image data (pixel array) can be found from the bytes at the offset +", offset, "bytes."
    # FIXME: on some images (like marbles.bmp), the offset we read seems to be wrong.

    # We now read the Windows BITMAPINFOHEADER (cf. Wikipedia)
    bitmap_width, = struct.unpack('<i', header[18:22])  # 4-byte integer
    bitmap_height, = struct.unpack('<i', header[22:26])  # 4-byte integer
    bits_per_pixel, = struct.unpack('<H', header[28:30])  # short integer
    assert bits_per_pixel == 24  # here we are reading a color image !

    print "According to its header, the image has a width of", bitmap_width, "pixels, and a height of", bitmap_height, "pixels."
    print "And each pixel uses", bits_per_pixel, "bits, ie", bits_per_pixel/8, "bytes : so it is three integer value from 0 to 2 **", bits_per_pixel/3, "- 1, ie 0 to", ((2**(bits_per_pixel/3))-1), "for", 2**(bits_per_pixel/3), "different colours/scales of (R,G,B)."

    # Checking that the image is indeed a pixel array of this size
    assert bitmap_width * bitmap_height * 3 == size_bytes - offset


    # %% Reading the content (after the offset)
    bigstring = "".join(content)[offset:]
    n = len(bigstring)
    #print bigstring
    print "We have", n, "bytes in this pixel array."

    # Now we have one big string, representing the pixel array


    # %% Trying to convert from bytes to integer

    # In our "lena_c.png" example:
    # The 8-bit per pixel (8bpp) format supports 256 distinct colors and stores 1 pixel per 3 bytes (R, G, B).

    pixels = list(struct.unpack('<' + 'BBB'*(n/3), bigstring))
    # RGB colors (3 8-bits integer, ie. 3 1-byte integer)
    # More details https://docs.python.org/2/library/struct.html?highlight=struct%20module#format-characters

    assert all(0 <= p < 2**(bits_per_pixel/3) for p in pixels)
    #print pixels

    # We need to be cautious
    # Normally pixels are stored "upside-down" with respect to normal image raster scan order, starting in the lower left corner, going from left to right, and then row by row from the bottom to the top of the image.

    mat_pixels_c = [[(pixels[2 + 3*j + i*3*bitmap_height], pixels[1 + 3*j + i*3*bitmap_height], pixels[3*j + i*3*bitmap_height]) for j in xrange(bitmap_height)] for i in xrange(bitmap_width-1, -1, -1)]
    # The order on the file is blue, green and red
    # But we store it as R, G, B

    mat_pixels = [[rgb_to_gray(c) for c in row] for row in mat_pixels_c]
#    print mat_pixels

    # We just check that it has been read correctly
    plt.figure()
    plt.title("Colored Lena \n(manually read from the file 'lena_c.bmp')")

    # Here there is two solutions, either:
    # 1. we convert the matrix of (r, g, b) to a NumPy array, of type uintf8 (universal integer on 8-bits, 0 <= < 2**8 = 256)
#    from numpy import array, uint8
#    plt.imshow(array(mat_pixels_c, dtype=uint8), cmap=None)

    # 2. we normalize the matrix so that each of its components r, g, b is a float number 0 <= < 1.
    mat_pixels_c_norm = [[[comp / 255.0 for comp in c] for c in row] for row in mat_pixels_c]
    plt.imshow(mat_pixels_c_norm)

    plt.savefig("lena_c_manually_read.png")


    plt.figure()
    plt.title("Gray-scale Lena \n(manually read and converted to gray-scale, from the file 'lena_c.bmp')")
    plt.imshow(mat_pixels, cmap='gray')
    plt.savefig("lena_c_gray_manually_read.png")

    return mat_pixels


# %% Test it
if __name__ == '__main__':
    lena_matrix = bmp_to_matrix("lena_c.bmp")
#    marbles_matrix = bmp_to_matrix("marbles.bmp")
