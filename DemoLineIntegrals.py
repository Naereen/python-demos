#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Examples of line integrals, computed with SymPy and Python.
Reference: https://docs.sympy.org/dev/modules/integrals/integrals.html#sympy.integrals.line_integrate

@date: Fri Feb 06 14:59:56 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

from sympy import *
from sympy.abc import x, y, t

# %% Example 1
# URL: http://perso.crans.org/besson/makebigmaths/?tex=%5Ctext%7BFor%20the%20function%7D~%20f(x%2C%20y)%20%3D%20%5Bx%2C%20y%5D%20%5C%3B%20%3A%20%5C%5C%20%5Ctext%7BLine%20integral%3A%7D~%20%5Coint_C%20f%20.%20dr%20%3D%20%3F&size=2.4

C1 = Curve([E**t + 1, E**t - 1], (t, 0, ln(2)))

int1 = line_integrate(x + y, C1, [x, y])
print r"For $f(x, y) = x + y$, the line integral $\oint_C f . dr $ is computed:", int1
pprint(int1)


# %% Example 2 : Exercise 1.a) of the Tutorial sheet 2
# URL: http://perso.crans.org/besson/makebigmaths/?tex=%5Ctext%7BFor%20the%20function%7D~%20f(x%2C%20y)%20%3D%20%5Bxy%2C%20y%5E2%5D%2C%20%5C%5C%20%5Ctext%7BThe%20line%20integral%20is%3A%7D~%20%5Coint_C%20f%20.%20dr%20%3D%20~%3F&size=2

C2 = Curve([cos(t), sin(t)], (t, pi, 0))
f = lambdify((x, y), [x*y, y**2])
int2 = line_integrate(f(x, y), C2, [x, y])
print r"For $f(x, y) = [xy, y^2]$, the line integral $\oint_C f . dr $ is computed:", int2
pprint(int2)


