#!/usr/bin/env make
# Author:	Lilian BESSON
# Ecsil:	lilian DOT besson AT ens-cachan D O T org
# Version:	2
# Date:		09-03-15
#
# Makefile for the 'python-demos' small project
#
###############################################################################
# Custom items
CP = /usr/bin/rsync --verbose --compress --human-readable --progress --archive --delete
#CP = scp
CP = /home/lilian/bin/CP --delete
GPG = gpg --no-batch --use-agent --detach-sign --armor --quiet --yes

git:
	git add README.md Makefile
	git commit -m "Auto commit with 'make git'."
	git push

send_zamok:
	$(CP) /home/lilian/Dropbox/python-demos.zip* besson@zamok.crans.org:~/www/dl/
	$(CP) /home/lilian/python-demos/* besson@zamok.crans.org:~/www/python-demos/

send_dpt:
	$(CP) /home/lilian/Dropbox/python-demos.zip* lbesson@ssh.dptinfo.ens-cachan.fr:~/public_html/dl/
	$(CP) /home/lilian/python-demos/* lbesson@ssh.dptinfo.ens-cachan.fr:~/public_html/python-demos/

zip:
	cd /home/lilian/ ; zip -r -9 /home/lilian/Dropbox/python-demos.zip ./python-demos/*
	$(GPG) /home/lilian/Dropbox/python-demos.zip
