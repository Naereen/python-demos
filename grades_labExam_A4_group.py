#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of PyLab for doing easy plotting for CS101 grades.
Way easier and nicer than Excel...

Please work on it by yourself to pick up bases of plotting with Python and PyLab.


@date: Wed Feb 25 17:38:00 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

# We import the PyLab module, which turns out Python as a MatLab clone
from pylab import *

# List of grades, included row from the Excel sheet
grades_from_excel = [
    16,
    2.5,
    5,
    14,
    16.5,
    12,
    8,
    10,
    17.5,
    7.5,
    1,
    7,
    16,
    7,
    18,
    9,
    19,
    20,
    11,
    6,
    13,
    12,
    7.5,
    1.5,
    10]

print "Average is:", sum(grades_from_excel)/len(grades_from_excel)
print "Top is:", max(grades_from_excel)
print "Bottom is:", min(grades_from_excel)



# Number-1 of parts of the pie chart. 10 is good
nb = 20

# We create a dictionnary, to count how many students were in each range [0%, 10%), [10%, 20%) etc
count = {nb*i: 0 for i in range(0, nb+1)}
# This uses dict comprehension, cf. https://www.python.org/dev/peps/pep-0274/ for more details


# We convert the grades into integers 0, 1, .., 10
# HINT: I multiply by 5 to scale a grade on 20 to a grade on 100 (the program is designed to accept grades from 0 to 100)
grades = [nb*(floor(5*g/nb)) for g in grades_from_excel]
# This uses list comprehension, cf. https://www.python.org/dev/peps/pep-0202/
# Or https://en.wikipedia.org/wiki/List_comprehension#Python for one simple example


# And we count
for g in grades:
    count[g] += 1  # One more student had his grade in THAT range [g, g+1)

# We display the result of this step
# The order of this for loop is random, it will NOT print [0, 10), [10, 20) etc.
for g, c in count.items():
    print c, "students got a mark between", g, "% and", g+1, "%."

    # We remove ranges with no students,
    # As we do not want to print empty parts on the pie chart
    if c == 0:
        count.pop(g)


# I want the parts in the pie chart to be sorted, so let us sort the keys
keys = count.keys()
keys.sort()  # This method will sort IN-PLACE the list keys

# Create the labels, again with list comprehension
# And string formatting (see https://docs.python.org/2/library/stdtypes.html#str.format)
labels = ["{} marks in [{}%, {}%]".format(count[k], k, k+10) for k in keys]


# Create the data that will be plotted
x = [count[k] for k in keys]  # one more example of list comprehension !


# Create the range of colors (grey) used for the pie chart
from matplotlib.colors import ColorConverter
cc = ColorConverter()  # technical boring step
# More details on http://matplotlib.org/api/colors_api.html#matplotlib.colors.ColorConverter.to_rgb

colors = [cc.to_rgb(str(1.0-k/100.0)) for k in keys]  # this will produce a scale of gray


# Now we (finally) make the pie chart
fig = figure(1)

# With a title
title("Pie chart representation of the performance for CS101 First Lab Exam (A4 group).")


pie(x,  # Using the list x as data
    labels=labels,  # The labels we created
    autopct="%i%%",  # A format string to be used for writing a label inside each wedge
    explode=[0.1]*len(x),  # Explode the pie chart
    colors=colors)  # And we use that range of gray colors

# Finally, we save the plot to a PNG image
savefig("Pie_chart_representation_of_the_performance_for_CS101__First_Lab_Exam__A4_group.png")
# And a PDF document
savefig("Pie_chart_representation_of_the_performance_for_CS101__First_Lab_Exam__A4_group.pdf")

print "We are done plotting, and two files has been produced in the current directory (one .png and one .pdf)."
print "An exercise for you can be to produce a colored drawing (change the way the colors list is created)."

print "Another exercise could be to generate a bar plot also."
fig = figure(2)
title("Bar representation of the performance for CS101 First Lab Exam (A4 group).")
xlabel("Grades between 0% and 100%.")
ylabel("Number of students with that grades.")
ylim(0, max(x)+1)

bar(keys,  # this is the data for x axis
    x, # this is the data for y axis
    color=colors,
    width=nb*0.9)
for xpixel, ypixel in zip(keys, x):
    text(xpixel+nb/2, ypixel+0.05, '%i students' % ypixel, ha='center', va='bottom')

# Finally, we save the second plot to a PNG image
savefig("Bar_representation_of_the_performance_for_CS101__First_Lab_Exam__A4_group.png")
# And a PDF document
savefig("Bar_representation_of_the_performance_for_CS101__First_Lab_Exam__A4_group.pdf")
