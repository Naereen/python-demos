#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Examples of plotting with MatPlotLib to illustrate Riemann left and right rectangles, and Trapezoidal methods for computing definite integrals of regular functions (of R -> R).

Note: Thanks to Niraj S. and Noel T. (B1) for pointing out a mistake (I was dividing by n-1 instead of n).

@date: Sun Apr 05 15:00:27 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

#from pylab import *
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    print "Plotting and computing the integral of cos(x) from -4 to 4."
    print "Formally, the value is about -1.5136049 (= 2*sin(4))."


# %% Riemann left rectangles rule

def plot_riemann_left(f, xmin, xmax, name_f, n=10):
    """ Plot the function f from xmin to xmax, and n Riemann left rectangles."""
    plt.figure()
    xvalues = np.linspace(xmin, xmax, 1000)
    yvalues = [f(x) for x in xvalues]
    plt.plot(xvalues, yvalues, 'r-')  # plots the function as a red line
    # Now plot the rectangles
    h = (xmax - xmin) / float(n)
    xi = [xmin + i*h for i in xrange(0, n)]
    # Note that arange(xmin, xmax, n) does the same
    yi = [f(x) for x in xi]  # left rectangles!
    for x, y in zip(xi, yi):
        plt.plot([x, x, x+h, x+h, x], [0, y, y, 0, 0], 'b-')
    # Finally, computing the area
    area = round(h * sum(yi), 4)
    plt.title("Riemman left rectangles for " + name_f + " with " + str(n) + " rectangles. Area is " + str(area))
    plt.savefig("riemannleft.png", dpi=120)
    return area


if __name__ == '__main__':
    # One example
    print "With left rectangles:", plot_riemann_left(np.cos, -4, 4, "cos", n=15)


# %% Riemann right rectangles rule

def plot_riemann_right(f, xmin, xmax, name_f, n=10):
    """ Plot the function f from xmin to xmax, and n Riemann right rectangles."""
    plt.figure()
    xvalues = np.linspace(xmin, xmax, 1000)
    yvalues = [f(x) for x in xvalues]
    plt.plot(xvalues, yvalues, 'r-')  # plots the function as a red line
    # Now plot the rectangles
    h = (xmax - xmin) / float(n)
    xi = [xmin + i*h for i in xrange(0, n)]
    # Note that arange(xmin, xmax, n) does the same
    yi = [f(x+h) for x in xi]  # right rectangles!
    for x, y in zip(xi, yi):
        plt.plot([x, x, x+h, x+h, x], [0, y, y, 0, 0], 'b-')
    # Finally, computing the area
    area = round(h * sum(yi), 4)
    plt.title("Riemman right rectangles for " + name_f + " with " + str(n) + " rectangles. Area is " + str(area))
    plt.savefig("riemannright.png", dpi=120)
    return area


if __name__ == '__main__':
    # One example
    print "With right rectangles:", plot_riemann_right(np.cos, -4, 4, "cos", n=15)


# %% Riemann center rectangles rule

def plot_riemann_center(f, xmin, xmax, name_f, n=10):
    """ Plot the function f from xmin to xmax, and n Riemann center rectangles."""
    plt.figure()
    xvalues = np.linspace(xmin, xmax, 1000)
    yvalues = [f(x) for x in xvalues]
    plt.plot(xvalues, yvalues, 'r-')  # plots the function as a red line
    # Now plot the rectangles
    h = (xmax - xmin) / float(n)
    xi = [xmin + i*h for i in xrange(0, n)]
    # Note that arange(xmin, xmax, n) does the same
    yi = [f(x + h/2.0) for x in xi]  # center rectangles!
    for x, y in zip(xi, yi):
        plt.plot([x, x, x+h, x+h, x], [0, y, y, 0, 0], 'b-')
    # Finally, computing the area
    area = round(h * sum(yi), 4)
    plt.title("Riemman center rectangles for " + name_f + " with " + str(n) + " rectangles. Area is " + str(area))
    plt.savefig("riemanncenter.png", dpi=120)
    return area


if __name__ == '__main__':
    # One example
    print "With center rectangles:", plot_riemann_center(np.cos, -4, 4, "cos", n=15)


# %% Trapezoidal rule

def plot_trapez(f, xmin, xmax, name_f, n=10):
    """ Plot the function f from xmin to xmax, and n trapezoides (as used for the Trapezoidal rule)."""
    plt.figure()
    xvalues = np.linspace(xmin, xmax, 1000)
    yvalues = [f(x) for x in xvalues]
    plt.plot(xvalues, yvalues, 'r-')  # plots the function as a red line
    # Now plot the trapezoides
    h = (xmax - xmin) / float(n)
    xi = [xmin + i*h for i in xrange(0, n)]
    # Note that arange(xmin, xmax, n) does the same
    # trapezoides !
    for x in xi:
        plt.plot([x, x, x+h, x+h, x], [0, f(x), f(x+h), 0, 0], 'b-')  # trapez
        plt.plot([x, x, x+h, x+h, x], [0, 0.5 * (f(x) + f(x+h)), 0.5 * (f(x) + f(x+h)), 0, 0], 'g:')  # equivalent rectangle
    # Finally, computing the area
    yi = [0.5 * (f(x) + f(x+h)) for x in xi]
    area = round(h * sum(yi), 4)
    plt.title("Trapezoidal rule for " + name_f + " with " + str(n) + " trapezoides. Area is " + str(area))
    plt.savefig("trapezoides.png", dpi=120)
    return area


if __name__ == '__main__':
    # One example
    print "With trapez:", plot_trapez(np.cos, -4, 4, "cos", n=15)

# Done
