#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Finding the first 2015 prime numbers:

 - with Python for and while loops,
 - with map,filter etc.
 - with a memoized optimization (Didier Clouteau's idea)

@date: Sat Jan 31 22:48:55 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

import math

# %% Lambda version, pretty
printFirst = lambda nb: "\n".join(
    map(
        lambda (i,m):"The {}th prime number is {}.".format(i+1,m),enumerate(
        filter(lambda n:
            all(
                map(
                    lambda k: ((n % k)!=0),
                    xrange(2,int(1+n**.5))
                    )
                ),
            xrange(2,int(nb*(math.log(nb)+math.log(math.log(nb)))))
        )[:nb]
    ))
  )
print printFirst(2015)


# %% Bound for p_n
def boundPn(n, verb=False):
    """ Use Pierre Dusart's formula to find an approximative bound for p_n, the nth prime number

    Source: http://www.ams.org/journals/mcom/1999-68-225/S0025-5718-99-01037-6/S0025-5718-99-01037-6.pdf
    Wikipedia: https://fr.wikipedia.org/wiki/Formules_pour_les_nombres_premiers#cite_note-3
    """
    assert(n>0)
#    if n<3: return (1, 3)
    m = max(n,2)
    from math import log as l
    x = n*(l(m)+l(l(m)))
    a = int(x-n)
    b = int(x-.5*n)
    if verb: print "The {n}th prime number p_{n} is between {a} and {b} (thanks to Pierre Dusart formula).".format(n=n, a=a, b=b)
    return (a, b)


[ boundPn(i, verb=True) for i in xrange(1,20) ]

# 7919, est compris entre 7840 et 8341.
boundPn(1000, verb=True)

def checkBound(n, pn):
    a, b = boundPn(n)
    return a <= pn <= b


# %%  One liner version, ugly
# We use Pierre Dusart's formula for the upper bound
import math;l=lambda  p:math.log(p,2);x=xrange;print(lambda n:"\n".join(map(lambda (i,m):"The {}th prime number is {}.".format(i+1,m),enumerate(filter(lambda n:all(map(lambda k: ((n%k)!=0),x(2,int(1+n**.5)))),x(2,int(n*(l(n)+l(l(n))-.5))))[:n]))))(2015)


# %% Clean version
def isPrime(n):
    """ Basic implementation with a for and if loops, and break construct."""
    result = True
    for k in xrange(2, int(math.sqrt(n))+1):
        if (n % k) == 0:
            # k is a divisor of n ==> n is not prime!
            result = False
            break  # Exit the for loop right now
    return result

nbprime = 2015
# Start it
print "2 is the first prime number. Let's find the next", nbprime-1, "ones."

i = 3
nb = 2
# As long as we do not have enough prime numbers
while nb < nbprime:
    assert (checkBound(nb, i))
    print i, "is the", nb, "th prime number."
    i += 2
    while not(isPrime(i)):
        i += 2
    nb += 1

print i, "is the last (the ", nb, "th one) prime number of our list."

