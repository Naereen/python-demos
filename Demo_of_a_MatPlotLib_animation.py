#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demonstration of a MatPlotLib animation.

References:
 - https://jakevdp.github.io/mpl_tutorial/tutorial_pages/tut6.html
 - http://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/
 - http://matplotlib.org/api/animation_api.html

@date: Tue Apr 21 10:03:21 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation


# %% First set up the figure, the axis, and the plot element we want to animate
fig, ax = plt.subplots()
a, b = -1.25, 1.25
ax.set_xlim(a, b)
ax.set_ylim(a, b)

plt.title("Animation of a moving sine wave (like in an oscilloscop).")
plt.xlabel("Time $t$")
plt.ylabel(r"$\sin(t)$ (translated)")

n = 400
x = np.linspace(a, b, n)
y = np.zeros_like(x)
t = np.linspace(0, 2*np.pi, n)
# n points between 0 and 2pi

line, = ax.plot(x, y, linewidth=2)


# Initialization function: plot the background of each frame
def init():
    line.set_data([], [])
    return line,


# Animation function.  This is called sequentially
def animate(i):
    y = np.sin(2 * np.pi * (x - 0.01 * i))
    line.set_data(x, y)
    return line,


# Call the animator.
# blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(
    fig, animate,
    init_func=init,
    frames=200, interval=20,
    blit=True)


# %% Save the animation as an mp4.
# This requires ffmpeg or mencoder to be installed.
# The codec argument ensure that the x264 codec is used, so that the video can be embedded in html5.
# You may need to adjust this for your system:
# for more information, see http://matplotlib.sourceforge.net/api/animation_api.html
#
#writer = animation.MovieWriter(fps=30, codec='libx264')
#
# anim.save('Demo_of_a_MatPlotLib_animation__sine_wave.mp4',
#           fps=30,
# #          codec='libx264',
# #          writer=writer
# #          writer='mencoder'
#           writer='ffmpeg'
#           )

# FIXME it fails on my Windows laptop?
# But it worked nicely on Ubuntu (with ffmpeg as a symlink to avconv, its official fork)


# %% Second example
fig, ax = plt.subplots()
a, b = -1.25, 1.25
ax.set_xlim(a, b)
ax.set_ylim(a, b)

n = 400
x = np.linspace(a, b, n)
y = np.zeros_like(x)
t = np.linspace(0, 2*np.pi, n)
# n points between 0 and 2pi

line, = ax.plot(x, y, linewidth=2)
plt.title(r"Curves $\Gamma_{i} = (\cos(t)^{i}, \sin(t)^{i})$ for some values i.")

# Initialization function: plot the background of each frame
def init():
    line.set_data([], [])
    line.set_linewidth(5)
    return line,


# Powers for the second example
powers = [ i/5.0 for i in xrange(0, 51) ]
powers += [ j for j in xrange(51, 61) ]
powers += [ i for i in xrange(1, 11) ] * 3
powers.sort()


# Animation function.  This is called sequentially
def animate(index):
    i = powers[index]
    title = r"Curve $\Gamma_{i} = (\cos(t)^{i}, \;\sin(t)^{i})$.".format(i=i)
    print title
    plt.legend(["i = {}".format(i)])

    x, y = np.cos(t)**i, np.sin(t)**i
    line.set_data(x, y)
    if i % 2 == 1:
        line.set_color("orange")
    elif int(i) == i:
        line.set_color("green")
    else:
        line.set_color("blue")
    return line,


# Call the animator.
anim = animation.FuncAnimation(
    fig, animate,
    init_func=init,
    frames=len(powers), interval=200,
    repeat=True,
    blit=True)


# %% Conclude by saving the animation
# anim.save('Demo_of_a_MatPlotLib_animation__shrinking_stars.mp4',
#           fps=5,
#           writer='ffmpeg'
#           )

# End
