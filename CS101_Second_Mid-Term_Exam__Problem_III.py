#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Commented solution for problem III for the CS101 second mid-term Exam.

All the comments are here to help you understand, and none of them were required during the exam.

The detailed marking scheme is also specified here.
WARNING: this marking scheme is approximative, and for some of you, I took the liberty of removing or adding a few marks (when it was appropriate).

Obviously, all these functions can be written in *many* different way, here is only one proposal (the simpler / more elegant).

Examples and tests are included in the end of the program.


@date: Thu Mar 26 22:04:54 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""


# %% Question Q.III.1)

# Remark : sorting the list and then selection l[0] or l[-1]
# was not an acceptable answer. Sorting is O(n log n) (or O(n2))
# but min/max can easily be computed with a single for loop in O(n)

# 1 for the max function, 1 for the min function
# 1 for handling the empty list [] correctly

def min(l):
    """ Compute the minimum value of the iterable l (can be a list or a tuple).

    - Time complexity : O(len(l)) for a list l.
    - Raise a ValueError exception for an empty list.
    - Note: Spyder (or pylint/flakes/..) might display a warning like : W0622 Redifining built-in 'min', because that name min is already taken.
    """
    if len(l) == 0:
        raise ValueError("min() arg is an empty sequence")
    else:
        smallest_yet = l[0]  # None also works
        for value in l:
            if smallest_yet > value:
                smallest_yet = value
        return smallest_yet


def max(l):
    """ Compute the maximum value of the iterable l (can be a list or a tuple).

    - Time complexity : O(len(l)) for a list l.
    - Raise a ValueError exception for an empty list.
    - Note: Spyder (or pylint/flakes/..) might display a warning like : W0622 Redifining built-in 'max', because that name max is already taken.
    """
    if len(l) == 0:
        raise ValueError("max() arg is an empty sequence")
    else:
        largest_yet = l[0]  # None also works
        for value in l:
            if largest_yet < value:
                largest_yet = value
        return largest_yet


# %% Question Q.III.2)
# A similar function for the product has been seen in the lecture about lists

# 1 for the sum function
# 1 for handling the empty list [] correctly

def sum(l):
    """ Compute the sum of the values of the iterable l (can be a list or a tuple).

    - Time complexity : O(len(l)) for a list l.
    - Returns 0 for an empty list.
    - Note: Spyder (or pylint/flakes/..) might display a warning like : W0622 Redifining built-in 'sum', because that name sum is already taken.
    """
    s = 0  # Takes care of the empty list
    for value in l:
        s += value
    return s


# %% Question Q.III.3)
# This was included in lab week 9 and 10 (and complete solution was provided on Moodle)
# A small typo in the problem confuses x and l, but none of you have been confused by this

# 1.5 for the average function
# 1 for being cautious about integer division (sum(l) / float(len(l)))
# 0.5 for handling the empty list [] correctly

def average(x):
    """ Compute the sum of the values of the iterable x (can be a list or a tuple).

    - Time complexity : O(len(x)) for a list x.
    - Returns 0 for an empty list.
    """
    if len(x) == 0:
        return 0
        # Remark: there is no uniform convention for this value 0
        # You could also do
        # raise ValueError("average() arg is an empty sequence")
    else:
        # We are cautious about integer division !
        return float(sum(x)) / float(len(x))


# %% Question Q.III.4)

# 2 for computing the sum of the product list x[k]*w[k]
# 2 for the function weighted_average
# 1 for being cautious about integer division
# +1 bonus point for being cautious about len(x) == len(w) or not (0.5 if you printed a message or did not compute the w.avg. and 1 if you raised an exception)

def oneone_product(x, w):
    """ Uses an easy list comprehension to compute the list of the products of values x[i] times w[i].

    - Will fail (AssertionError) if x and w have different sizes.
    - Time complexity : O(n) for two lists x and w of size n.
    """
    assert len(x) == len(w)
    return [ wvalue * xvalue for xvalue, wvalue in zip(x, w) ]


def weighted_average(x, w):
    """ Compute the weigthed average of the values x with the weight w.

    - Time complexity : O(n) for two lists x and w of size n.
    - Returns 0 for an empty list.
    """
    if len(w) == 0:
        return 0
    else:
        # We check that all w[i] > 0 (was not asked)
        assert all( [ wvalue > 0 for wvalue in w ] )
        # We are cautious about integer division !
        return float(sum(oneone_product(x, w))) / float(sum(w))

def average2(x):
    """ Simple re-implementation of the average function thanks to the weighted_average one.

    - Remark: this was NOT asked in the exam.
    """
    return weighted_average(x, [1]*len(x))
    # [1] * n is [1, 1, ..., 1] of size n, as seen in class
    # And if n is 0, then [1] * n is [] an empty list


# %% Question Q.III.5)

# average(l) is in O(n)
# weighted_average(l) is also in O(n)

# 3/5 marks for the answer, 2/5 for a quick justification
# Remark: CS is a science, when we need to justify (even quickly) what we do!
# Especially when it is an analytic question like this one


# %% Question Q.III.6)

# 2.5/5 for the geom_mean function (1.5 for the function, 0.5 for [] and 0.5 for being efficient in O(n))
# 2.5/5 for the harmo_mean function (1.5 for the function, 0.5 for [] and 0.5 for being efficient in O(n))

def prod(l):
    """ Compute the product of the values of the iterable l (can be a list or a tuple).

    - Time complexity : O(len(l)) for a list l.
    - Returns 1 for an empty list.
    """
    p = 1  # Takes care of the empty list
    for value in l:
        p *= value
    return p


def geom_mean(x):
    """ Compute the geometrical mean of the positive values x.

    - Time complexity : O(n) for one list x size n.
    - Returns 0 for an empty list.
    """
    if len(x) == 0:
        return 1  # Convention
    else:
        # We check that all x[i] > 0 (was not asked)
        assert all( [ value > 0 for value in x ] )
        # We are cautious about integer division !
        return float( prod(x) ) ** (1.0 / len(x))


def harmo_mean(x):
    """ Compute the harmonic mean of the positive values x.

    - Time complexity : O(n) for one list x size n.
    - Returns 0 for an empty list.
    """
    if len(x) == 0:
        return 0  # Convention
    else:
        # We check that all x[i] > 0 (was not asked)
        assert all( [ value > 0 for value in x ] )
        # We are cautious about integer division !
        return float( len(x) ) / float( sum( [ 1.0/value for value in x] ) )



# %% Bonus : Weighted GM and HM
# This was NOT asked in the exam, but was initially the last question
# https://en.wikipedia.org/wiki/Weighted_geometric_mean

def weigthed_prod(x, w):
    """ Compute the weighted product of the values of the list x with the weight w : $prod_{k=0}^{n-1} x_k * w_k$.

    - Time complexity : O(n) for two lists x and w of size n.
    - Returns 1 for an empty list.
    """
    assert len(x) == len(w)
    p = 1  # Takes care of the empty list
    for xvalue, wvalue in zip(x, w):
        p *= (xvalue ** wvalue)
    return p


def weighted_geom_mean(x, w):
    """ Compute the weighted geometrical mean of the positive values x with the weight w.

    - Time complexity : O(n) for two lists x and w of size n.
    - Returns 0 for an empty list.
    """
    if len(x) == 0:
        return 1  # Convention
    else:
        # We check that all x[i] > 0 and w[i] > 0
        assert all( [ xvalue > 0 for xvalue in x ] )
        assert all( [ wvalue > 0 for wvalue in w ] )
        # We are cautious about integer division !
        return float( weigthed_prod(x, w) ) ** (1.0 / sum(w))


def weighted_harmo_mean(x, w):
    """ Compute the weighted harmonic mean of the positive values x with the weight w.

    - Time complexity : O(n) for two lists x and w of size n.
    - Returns 0 for an empty list.
    """
    if len(x) == 0:
        return 1  # Convention
    else:
        # We check that all x[i] > 0 and w[i] > 0
        assert all( [ xvalue > 0 for xvalue in x ] )
        assert all( [ wvalue > 0 for wvalue in w ] )
        # We are cautious about integer division !
        return float( sum(w) ) / float( sum( [ 1.0/(xvalue*wvalue) for xvalue, wvalue in zip(x, w) ] ) )



# %% We test all these functions on small list
# None of these tests were asked during the exam

if __name__ == '__main__':
    l = [9, 2, -3, 5]
    assert min(l) == -3
    assert max(l) == 9
    assert sum(l) == 13
    assert average(l) == 13.0 / 4
    w = [1, 2, 1, 2]
    assert weighted_average(l, w) == 10.0 / 3


    # More example of averages, as given in the paper
    assert average([0, 1]) == 0.5
    assert average([1, 2, 3, 4, 5]) == 3.0
    assert average([-1, 1, 3]) == 1.0


    x = [1, 2, 3]
    assert average2(x) == 2
    assert geom_mean(x) == (6 ** (1.0 / 3.0))  # 1.8171205928321397
    assert harmo_mean(x) == (18. / 11.)  # 1.6363636363636365

    w = [2, 1, 1]
    assert weighted_average(x, w) == (7.0 / 4)
    assert weighted_geom_mean(x, w) == 1.5650845800732873
    assert weighted_harmo_mean(x, w) == 3.0

    # Let us verify the AM>GM inequality (with a big random list)
    #  https://en.wikipedia.org/wiki/Inequality_of_arithmetic_and_geometric_means#The_inequality
    from random import randint
    x = [ randint(1, 100) for i in xrange(100) ]
    assert average(x) >= geom_mean(x)

    print "CS101 Second Mid-Term Exam : Problem III : all tests have been passed successfully."