#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of SymPy to compute formal determinant (with variables and not numbers).

The examples are coming from MA102 Tutorial Sheet 8 (and 7), given on Monday the 30th of March 2015.


@date: Fri Mar 27 09:57:09 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

from sympy import pprint, sin, cos, tan
from sympy.matrices import Matrix

# Variables
from sympy.abc import theta, a, b, c, d, e, f, g, h, i

# We use what Prof. Vipin taught last week
# This test will be True only if this program is executed directly
if __name__ == '__main__':
    print "Starting some examples and tests (from MA102 Tutorial Sheet 8):"

    # Problem 1)
    print "\nProblem 1):"

    A = Matrix( [[cos(theta), sin(theta), tan(theta)], [0, cos(theta), sin(theta)], [0, -sin(theta), cos(theta)]] )
    print "This first matrix:"
    pprint(A)
    print "has a determinant equals to:"
    pprint(A.det())
    print "Which is simply:"
    pprint(A.det().simplify())

    B = Matrix( [[a, b, 0], [0, a, b], [a, 0, b]] )
    print "This second matrix:"
    pprint(B)
    print "has a determinant equals to:"
    pprint(B.det())

    C = Matrix( [[0, a, 0], [b, c, d], [0, e, 0]] )
    print "This third matrix:"
    pprint(C)
    print "has a determinant equals to:"
    pprint(C.det())

    # Problem 2)
    print "\nProblem 2):"

    A = Matrix( [[a, b, c], [d, e, f], [g, h, i]] )
    print "Working with this main matrix A:"
    pprint(A)
    print "has a determinant equals to:"
    pprint(A.det())

    print "First determinant is det(A1) = 2 det(A):"
    pprint(Matrix( [[2*a, 2*b, 2*c], [d, e, f], [g, h, i]] ).det())

    print "Second determinant is det(A2) = - det(A):"
    pprint(Matrix( [[d, e, f], [a, b, c], [g, h, i]] ).det())

    print "Third determinant is det(A3) = -2 det(A):"
    pprint(Matrix( [[2*c, b, a], [2*f, e, d], [2*i, h, g]] ).det())

    print "Fourth determinant is det(A4) = -6 det(A):"
    pprint(Matrix( [[3*a, -b, 2*c], [3*d, -e, 2*f], [3*g, -h, 2*i]] ).det())

    print "Fifth determinant is det(A5) = det(A):"
    pprint(Matrix( [[a+g, b+h, c+i], [d, e, f], [g, h, i]] ).det())

    print "Sixth determinant is det(A6) = 0:"
    pprint(Matrix( [[a+g, b+h, c+i], [g, h, i], [g, h, i]] ).det())


    # Sheet 7 Problem 12) a)
    print "\nSheet 7, problem 12.a):"
    A = Matrix( [[1, 2, -4], [-1, -1, 5], [2, 7, -3]] )
    pprint(A)
    pprint(A.inv())

    # Sheet 7 Problem 12) b)
    print "\nSheet 7, problem 12.b):"
    A = Matrix( [[1, 3, -4], [1, 5, -1], [3, 13, -6]] )
    pprint(A)
    try:
        pprint(A.inv())
    except ValueError:
        print "This matrix in not invertible."

    # Sheet 7 Problem 12) c)
    print "\nSheet 7, problem 12.c):"
    A = Matrix( [[1, 0, 2], [2, -1, 3], [4, 1, 8]] )
    pprint(A)
    try:
        pprint(A.inv())
    except ValueError:
        print "This matrix in not invertible."
