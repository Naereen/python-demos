#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of the scipy.optimize module for black-box optimization.

Online reference is http://scipy-lectures.github.io/advanced/mathematical_optimization/index.html


@date: Mon Mar 02 22:26:49 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

from pylab import *
from scipy import optimize

# %% 1D optimization with Brent method
def fexp(x):
    """ Example 1D function x -> -exp(-(x-.7)**2)."""
    return - exp(-(x - .7)**2)

print "\nFor the 1D function x -> -exp(-(x-.7)**2), we look for the (global) minimum:"
# It actually converges in 9 iterations!
x_min = optimize.brent(fexp)
print "scipy.optimize.brent found the value x_min =", x_min

x_min - .7

# %% 2D optimization with conjugate gradient descent method
def rosenbrock (x):
    """ The Rosenbrock function."""
    return .5*(1 - x[0])**2 + (x[1] - x[0]**2)**2

print "\nFor the 2D Rosenbrock function, we look for the (global) minimum,  with conjugate gradient descent method:"
# It actually converges in 13 iterations!
xy_min = optimize.fmin_cg(rosenbrock , [2, 2])

print "scipy.optimize.fmin_cg found the 2D point xy_min =", xy_min


# %% 2D optimization with conjugate gradient descent method, when we know the gradient
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

def rosenbrock_prime(xy):
    """ The gradient of the Rosenbrock function."""
    return np.array((-2*.5*(1 - xy[0]) - 4*xy[0]*(xy[1] - xy[0]**2), 2*(xy[1] - xy[0]**2)))

# One check, not done for each step
assert optimize.check_grad(rosenbrock, rosenbrock_prime, [2, 2]) < 1e-6


print "\nFor the 2D Rosenbrock function, we look for the (global) minimum,  with conjugate gradient descent method (but now we computed its gradient ourself!):"
# It actually converges in 13 iterations!
xy_min = optimize.fmin_cg(rosenbrock, [2, 2], fprime=rosenbrock_prime)

print "scipy.optimize.fmin_cg with this fprime found the 2D point xy_min =", xy_min


# %% 2D optimization with Newton's method, when we know the gradient
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

def rosenbrock_prime(xy):
    """ The gradient of the Rosenbrock function."""
    return np.array((-2*.5*(1 - xy[0]) - 4*xy[0]*(xy[1] - xy[0]**2), 2*(xy[1] - xy[0]**2)))


print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with Newton's method (but now we computed its gradient ourself!):"
# It actually converges in 9 iterations!
xy_min = optimize.fmin_ncg(rosenbrock, [2, 2], fprime=rosenbrock_prime)

print "scipy.optimize.fmin_ncg with this fprime found the 2D point xy_min =", xy_min


# %% 2D optimization with Newton's method, when we know the gradient and the Hessian
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

def rosenbrock_prime(xy):
    """ The gradient of the Rosenbrock function."""
    return np.array((-2*.5*(1 - xy[0]) - 4*xy[0]*(xy[1] - xy[0]**2), 2*(xy[1] - xy[0]**2)))

def rosenbrock_hess(xy):
    """ The Gradient of the Rosenbrock function."""
    return np.array(((1 - 4*xy[1] + 12*xy[0]**2, -4*xy[0]), (-4*xy[0], 2)))


print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with Newton's method (but now we computed its gradient and its Hessian ourself!):"
# It actually converges in 9 iterations!
xy_min = optimize.fmin_ncg(rosenbrock, [2, 2], fprime=rosenbrock_prime, fhess=rosenbrock_hess)

print "scipy.optimize.fmin_ncg with this fprime and fhess found the 2D point xy_min =", xy_min


# %% 2D optimization with BFGS's method, when we know the gradient
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

def rosenbrock_prime(xy):
    """ The gradient of the Rosenbrock function."""
    return np.array((-2*.5*(1 - xy[0]) - 4*xy[0]*(xy[1] - xy[0]**2), 2*(xy[1] - xy[0]**2)))


print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with BFGS's method (but now we computed its gradient ourself!):"
# It actually converges in 16 iterations!
xy_min = optimize.fmin_bfgs(rosenbrock, [2, 2], fprime=rosenbrock_prime)

print "scipy.optimize.fmin_bfgs with this fprime found the 2D point xy_min =", xy_min


# %% 2D optimization with L-BFGS's method
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with L-BFGS's method:"
# It actually converges in 16 iterations!
xy_min = optimize.fmin_l_bfgs_b(rosenbrock, [2, 2], approx_grad=1)[0]

print "scipy.optimize.fmin_l_bfgs_b found the 2D point xy_min =", xy_min


# %% 2D optimization with L-BFGS's method, when we know the gradient
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

def rosenbrock_prime(xy):
    """ The gradient of the Rosenbrock function."""
    return np.array((-2*.5*(1 - xy[0]) - 4*xy[0]*(xy[1] - xy[0]**2), 2*(xy[1] - xy[0]**2)))


print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with L-BFGS's method (but now we computed its gradient ourself!):"
# It actually converges in 16 iterations!
xy_min = optimize.fmin_l_bfgs_b(rosenbrock, [2, 2], fprime=rosenbrock_prime)[0]

print "scipy.optimize.fmin_l_bfgs_b with this fprime found the 2D point xy_min =", xy_min


# %% 2D optimization with Powell's method
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with Powell's method:"
# It actually converges in 11 iterations!
xy_min = optimize.fmin_powell(rosenbrock, [2, 2])

print "scipy.optimize.fmin found the 2D point xy_min =", xy_min


# %% 2D optimization with Nelder-Mead's method
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with Nelder-Mead's method:"
# It actually converges in 46 iterations!
xy_min = optimize.fmin(rosenbrock, [2, 2])

print "scipy.optimize.fmin found the 2D point xy_min =", xy_min


# %% 2D optimization with Brute force method (a grid search)
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with Brute force method:"
xy_min = optimize.brute(rosenbrock, ((-1, 2), (-1, 2)))

print "scipy.optimize.fmin found the 2D point xy_min =", xy_min
print "Note that this method is not precise, but can be a good way to find a starting point for one of the previous methods."



# %% 2D optimization with Brute force method (a grid search)
def rosenbrock(xy):
    """ The Rosenbrock function."""
    return .5*(1 - xy[0])**2 + (xy[1] - xy[0]**2)**2

print "\nFor the 2D Rosenbrock function, we look for the (global) minimum, with Brute force method:"
# It actually converges in 100 iterations!
xy_min = optimize.basinhopping(rosenbrock, ((-1, 2), (-1, 2)))

print "scipy.optimize.fmin found the 2D point xy_min =", xy_min.x
print "Note that this method is not precise, but can be a good way to find a starting point for one of the previous methods."


# %% Exercise 1 : A simple (?) quadratic function
def ex1():
    """ Random f, K for exercise 1."""
    np.random.seed(0)
    K = np.random.normal(size=(100, 100))

    def f(x):
        """ Multi-dimension function to optimize."""
        return np.sum((np.dot(K, x - 1))**2) + np.sum(x**2)**2

    return f, K

# %% Exercise 1, first try
f, K = ex1()
#%timeit optimize.fmin(f, K[0])
xv_min1 = optimize.fmin(f, K[0])
print "For Exercise 1 : with fmin:", xv_min1  # 1.98 second

# %% Exercise 1, second try
f, K = ex1()
#%timeit optimize.fmin_powell(f, K[0])
xv_min2 = optimize.fmin_powell(f, K[0])
print "For Exercise 1 : with fmin_powell:", xv_min2  # 201 mili-second

# %% Exercise 1, third try
f, K = ex1()
#%timeit optimize.fmin_bfgs(f, K[0])
xv_min3 = optimize.fmin_bfgs(f, K[0])
print "For Exercise 1 : with fmin_bfgs:", xv_min3  # 537 mili-second

# %% Exercise 1, last try
f, K = ex1()
#%timeit optimize.anneal(f, K[0])
xv_min4 = np.array(optimize.basinhopping(f, K[0])[0])
print "For Exercise 1 : with basinhopping:", xv_min4  # 14.6 mili-second


# %% Exercise 2 : A locally flat minimum
alpha, beta = 1.0, 0.1  # the x coordinate of the output is going to be fine, the y will be wrong
#alpha, beta = 1.0, 1.0  # the x coordinate of the output is going to be fine, the y will be wrong

def f(xy):
    """ A locally flat minimum."""
    return exp(-1/(alpha*xy[0]**2 + beta*xy[1]**2))

xtol, ftol = 1e-16, 1e-16

# %% Exercise 2, first try
xy_min = optimize.fmin(f, [1, 1], xtol=xtol, ftol=ftol)
print "For Exercise 2 : with fmin:", xy_min

# %% Exercise 2, second try
xy_min = optimize.fmin_powell(f, [1, 1], xtol=xtol, ftol=ftol)
print "For Exercise 2 : with fmin_powell:", xy_min

# %% Exercise 2, third try
xy_min = optimize.fmin_bfgs(f, [1, 1], epsilon=xtol, gtol=ftol)
print "For Exercise 2 : with fmin_bfgs:", xy_min

# %% Exercise 2, last try
xy_min = optimize.basinhopping(f, [1, 1]).x
print "For Exercise 2 : with basinhopping:", xy_min
