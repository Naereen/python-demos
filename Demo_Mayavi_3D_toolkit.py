#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Mayavi 3D toolkit demo.


Reference: Python Scientific lecture notes, Release 2013.2 beta (euroscipy 2013).

@date: Fri Apr 03 14:21:41 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

import numpy as np
import mayavi.mlab as mlab
#mlab.clf()

# %% Points in 3D
print "Example 1: Points in 3D, represented with markers (or “glyphs”) and optionaly different sizes."
fig = mlab.figure(1)

x, y, z, value = np.random.random((4, 40))
mlab.points3d(x, y, z, value)
mlab.title("Points in 3D")
mlab.savefig("Demo_Mayavi__Points_in_3D.png")


# %% 3D line
print "Example 2: A line connecting points in 3D, with optional thickness and varying color."
fig = mlab.figure(2)

t = np.linspace(0, 20, 200)
mlab.plot3d(np.sin(t), np.cos(t), 0.1*t, t)
mlab.title("3D line")
mlab.savefig("Demo_Mayavi__3D_line.png")


# %% Evaluation surface
print "Example 3: A surface given by its elevation, coded as a 2D array."
fig = mlab.figure(3)

# np.mgrid is SO DURTY!

# However, if the step length is a complex number (e.g. 5j), then the integer part of its magnitude is interpreted as specifying the number of points to create between the start and stop values, where the stop value is inclusive.
x, y = np.mgrid[-10:10:100j, -10:10:100j]  # pylint: disable=E1127

r = np.sqrt(x**2 + y**2)
z = np.sin(r)/r

mlab.surf(z, warp_scale='auto')
mlab.title("Evaluation surface")
mlab.savefig("Demo_Mayavi__Evaluation_surface.png")


# %% Arbitrary regular mesh
print "Example 4: A surface mesh given by x, y, z positions of its node points"
fig = mlab.figure(4)

phi, theta = np.mgrid[0:np.pi:11j, 0:2*np.pi:11j]  # pylint: disable=E1127
x = np.sin(phi) * np.cos(theta)
y = np.sin(phi) * np.sin(theta)
z = np.cos(phi)
mlab.mesh(x, y, z)
mlab.mesh(x, y, z, representation='wireframe', color=(0, 0, 0))
mlab.title("Arbitrary regular mesh")
mlab.savefig("Demo_Mayavi__Arbitrary_regular_mesh.png")


# %% Volumetric data
print "Example 5: If your data is dense in 3D, it is more difficult to display. One option is to take iso-contours of the data."
fig = mlab.figure(5)

x, y, z = np.mgrid[-5:5:64j, -5:5:64j, -5:5:64j]  # pylint: disable=E1127
values = x*x*0.5 + y*y + z*z*2.0
mlab.contour3d(values)
mlab.title("Volumetric data")
mlab.savefig("Demo_Mayavi__Volumetric_data.png")

