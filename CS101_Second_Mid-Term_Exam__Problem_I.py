#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Code for some questions of problem I for the CS101 second mid-term Exam.

This is here to allow you to execute by yourself the different snippets of code.

@date: Thu Mar 26 22:04:54 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""


# %% Question Q.I.6)

m = [ [1, 2, 3], [4, 5, 6], [7, 8, 9] ]
c = 0
for i in xrange(3):
    for j in xrange(3):
        if i==j:
            c = c + m[i][j]  # same as c += m[i][j]
print "Result is", c

# Remark: none of the answer was correct in fact, the output being
# Result is 15


# %% Question Q.I.7)

import math, sys, os
try:
    import file
except ImportError:
    print "The module file is not available."


# %% Question Q.I.8)

#from math.sqrt import
# The above line fails (SyntaxError: invalid syntax)


# %% Question Q.I.9)

import math
print "For Question Q.I.9) dir(math) =", dir(math)


# %% Question Q.I.10)

s = sum([ i for i in xrange(20) if (i % 2 == 0) and (i % 5 == 0) ])
print "For Question Q.I.10) s =", s


# %% Question Q.I.12)

l = []
l.append(2)
l.append(6)
l.append(8)
assert l == [2, 6, 8]


l = [8]
l.append(2)
l = [2] + l * 2
l.insert(1, 6)
l.pop()
print "For Question Q.I.12) b) the produced l is", l
assert l != [2, 6, 8]

l = [8]
l += [6]
l += [2]
print "For Question Q.I.12) c) the produced l is", l
assert l != [2, 6, 8]

l = list({2, 6, 8})  # non-deterministic !
l = l
l = list(l)  # [8, 2, 6] on my laptop
print "For Question Q.I.12) d) the produced l is", l
assert l != [2, 6, 8]


# %% Question Q.I.13)

# a)
s = {-1, 1, 0, -1, 1}  # works
# but d will just be {-1, 1, 0}, not a multi-set
print "For Question Q.I.13) a) the set s is", s

# b)
s = {2+3j, 0, 2-3j}  # works
print "For Question Q.I.13) a) the set s is", s

# c)
try:
    s = {-1, [0], 1}
    print "For Question Q.I.13) c) the set s is", s
except:
    print "For Question Q.I.13) c) the set failed to be defined."

# d)
try:
    s = {4, {-1, 1}, [4+6j]}
    print "For Question Q.I.13) d) the set s is", s
except:
    print "For Question Q.I.13) d) the set failed to be defined."



# %% Question Q.I.14)

d = { i : i**3 for i in range(12) }
print "For Question Q.I.14) b) the good dict d is", d


# %% Question Q.I.15)



