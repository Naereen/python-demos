#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demos and examples of the pylab environment.
Used for my lectures on Scientific plotting, April 2015.

More documentation online at https://github.com/rougier/matplotlib-tutorial.

@date: Wed Jan 21 16:01:45 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: GNU Public License version 3.
"""

# We import the pylab environment
import numpy as np
import matplotlib.pyplot as plt

# Default number of points for the plots
n = 200


# %% Test 1 (functions of x)
# Creates a new figure
plt.figure(1)

# Sample some points from -10 to 10
x = np.linspace(-40, 10, n)

# And finally, plot cos and sin
#plt.plot(x, np.cos(x), 'b+-', x, np.sin(x), 'kx')

plt.plot(x, np.cos(x), color='black', linewidth=2, linestyle='-', label="Black cosine")
plt.plot(x, np.sin(x), color='magenta', linewidth=8, linestyle=':', label="Magenta sine")

plt.legend()
plt.title("Cosinus and sinus on the same plot.")

#plt.savefig("DemoPylab__figure1.svg"
#plt.savefig("Demo_Pylab__Figure_1__Function_of_x.png", dpi=120)


# %% Test 2 (param. curve)
plt.figure(2)

plt.axis('equal')  # What is that?

t = np.linspace(0, 7*np.pi, n)

plt.plot(t - np.sin(t), 1 - np.cos(t), label='Cycloid')

plt.title(r"Plot of a cycloid (t between $0$ and $7\pi$)")
plt.legend()

#plt.savefig("DemoPylab__figure2.svg")
#plt.savefig("Demo_Pylab__Figure_2__Parametric_curve.png", dpi=120)


# %% Test 3 (param. curve 2)
plt.figure(3)

plt.axis('equal')

n = 2000
t = np.linspace(-100, 100, n)

plt.plot(4.0*t/(3+t**4), 4.0*(t**3)/(np.sqrt(3)*(3+t**4)))

plt.title("Example of a parametric curve (from MA102 Tutorial sheet)")

#plt.savefig("DemoPylab__figure3.svg")
#plt.savefig("Demo_Pylab__Figure_3__Parametric_curve_2.png", dpi=120)


# %% Test 4 (polar curve 1)
plt.figure(4)
plt.axis('equal')

theta = np.linspace(0, 2*np.pi, n)
r = np.cos(theta)-np.cos(2*theta)

mylabel = r"$r(\theta) = \cos(\theta) - \cos(2\theta)$"

plt.polar(theta, r, label=mylabel)

plt.legend(loc="lower left")  # change the location of the legend
plt.title("Example of one polar curve")

#plt.savefig("DemoPylab__figure4.svg"
#plt.savefig("Demo_Pylab__Figure_4__Polar_curve_1.png", dpi=120)

# Different formats:
# - PNG: Portable Network Graphic
# - JPEG: Joint Photographic Experts Group
# - PDF: Portable Document Format
# - SVG: Scalable Vector Graphic
# - More are available (depending on the platform)


# %% Test 5 (param. curve 3)
plt.figure(5)
plt.axis('equal')

t = np.linspace(0, 2*np.pi, n)  # n points between 0 and 2pi

# But what is that loop?
# 4 new figures, or 4 plots on the same figure?
for i in [1, 2, 3, 5, 1001]:
    plt.plot(np.cos(t)**i, np.sin(t)**i, label=("$i=%i$" % i), linewidth=3)

plt.legend()
plt.title(r"Curves $\Gamma_i = (\cos(t)^i, \sin(t)^i)$ for some values of $i$.")

#plt.savefig("DemoPylab__figure5.svg"
#plt.savefig("Demo_Pylab__Figure_5__Polar_curve_2.png", dpi=120)


# %% Test 6 (3D plotting)
# This is the 3D plotting toolkit (Ref. https://jakevdp.github.io/mpl_tutorial/tutorial_pages/tut5.html)
from mpl_toolkits.mplot3d import Axes3D

z = np.linspace(0, 1, 150)
x = z * np.sin(20 * z)
y = z * np.cos(20 * z)

plt.figure(6)
ax = plt.axes(projection='3d')
# Creating a 3D axes object
# then we use ax.plot or other methods,
# instead of plt.plot !

ax.plot(x, y, z, '-b')
plt.title("Line plot in 3D")

#plt.savefig("DemoPylab__figure6.svg"
plt.savefig("Demo_Pylab__Figure_6__3D_line_plot.png", dpi=120)

plt.figure(7)
ax = plt.axes(projection='3d')

#ax.plot(x, y, z, 'r^', linewidth=8)
ax.scatter(x, y, z, c=z, linewidth=0, s=100, cmap='hot')

plt.title("Scatter plot in 3D (color is depending on the position)")
#plt.savefig("DemoPylab__figure7.svg"
#plt.savefig("Demo_Pylab__Figure_7__3D_scatter_plot.png", dpi=120)


# %% Test 7 (3D plotting, surface)
plt.figure(8)
from mpl_toolkits.mplot3d import Axes3D  # useless to do it too many times

N = 20

# Create data for x and y axis
x = np.outer(np.linspace(-np.pi/2, np.pi/2, N), np.ones(N))  # outer product, cf the documentation
#x = np.array([ np.linspace(-np.pi/2, np.pi/2, N) for i in range(N) ])
y = x.copy().T

# Computing the z for a 3D plots require x and y to be of size N,N (matrix) and not just N
# z = np.cos(x ** 2 + y ** 2)
z = x ** 2 + y ** 2

ax = plt.axes(projection='3d')
ax.plot_surface(x, y, z, cmap='jet', rstride=1, cstride=1, linewidth=0)

plt.xlabel('x')
plt.ylabel('y')

# rstride: step size for the rows of the array
# cstride: step size for the cols of the array
# linewidth: put to 0 is more readable (no black line)

plt.title("Surface in 3D (an hyperboloid $z = x^2 + y^2$)")

#plt.savefig("DemoPylab__figure8.svg"
plt.savefig("Demo_Pylab__Figure_8__3D_plotting_surface.svg")


# %% Test 8 (More options for plotting)
plt.figure(9)

# x axis
n = 10000
x = np.linspace(-2*np.pi, 2*np.pi, n)
plt.xlim(np.min(x)*1.1, np.max(x)*1.1)  # FIXME not general enough
plt.xticks([-2*np.pi, -np.pi, -np.pi/2, 0, np.pi/2, np.pi, 2*np.pi], [r"$-2\pi$", r"$-\pi$", r"$-\pi/2$", r"$0$", r"$\pi/2$", r"$\pi$", r"$2\pi$"])

# y axis
C, S = np.cos(x), np.sin(x)
plt.ylim(C.min()*1.1, C.max()*1.1)
plt.yticks([-1, -0.5, 0.5, 1], [r"$-1$", r"$-1/2$", r"$1/2$", r"$1$"])

# Move spines
ax = plt.gca()
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data', 0))

# Plotting
plt.plot(x, C, color='blue', linewidth=1, linestyle='-', label=r'$\cos$')
plt.plot(x, S, color='red', linewidth=4, linestyle=':', label=r'$\sin$')

# Legend
plt.legend(loc='lower left')
plt.title("Plot of two functions, showing plenty of options")

# Make the tick labels appear nicer
for label in ax.get_xticklabels() + ax.get_yticklabels():
    label.set_fontsize(16)
    label.set_bbox(dict(facecolor='white', edgecolor='None', alpha=0.65))

# Save the figure
#plt.savefig("DemoPylab__figure9.svg")
#plt.savefig("Demo_Pylab__Figure_9__Lot_of_options.png", dpi=120)


# %% Test 10 (Scatter 2D points)
plt.figure(10)
N = 200

# Random points, uniformly taken in [-2, 2]
#X = np.random.uniform(-2, 2, N)
#Y = np.random.uniform(-2, 2, N)

# Random points, taken in [-2, 2] with a Normal law (Gaussian profile, centered on 0)
X = np.random.normal(-2, 2, N)
Y = np.random.normal(-2, 2, N)


plt.xlim(X.min(), X.max())
plt.ylim(Y.min(), Y.max())

colors = np.arctan2(Y, X)  # Depends on the angle!

plt.scatter(X, Y, alpha=0.6, c=colors, cmap='jet', linewidths=0, s=100)

# s=90 is the size**2 of the points.
# We need to specify the color map with cmap='jet', ='hot', ='winter' or the one you like.

plt.title("2D scatter plot of %i random points \n(colour depends on the polar angle)." % N)

#plt.savefig("DemoPylab__figure10.svg")
#plt.savefig("Demo_Pylab__Figure_10__2D_Scatter_points.png", dpi=120)


# %% Test 11 (Bar plots)
plt.figure(11)

n = 12
X = np.arange(n)

# Random Y data: 1 - X/n scaled with some noise
Y1 = (1-X/float(n)) * np.random.uniform(0.5, 1.0, n)
Y2 = (1-X/float(n)) * np.random.uniform(0.5, 1.0, n)

# Two bar plots, one positive, one negative
plt.bar(X, +Y1, facecolor='#9999ff', edgecolor='white')  # a light blue
plt.bar(X, -Y2, facecolor='#ff9999', edgecolor='white')  # a light red

# Colors can be given with a RGB hexadecimal code
# (as it is done in Web programming and CSS)

# Write values of each bar
for x, y in zip(X, Y1):
    plt.text(x+0.4, y+0.05, "%.2f" % y, ha='center', va='bottom')
for x, y in zip(X, -Y2):
    plt.text(x+0.4, y-0.05, "%.2f" % y, ha='center', va='top')

plt.ylim(-1.25, +1.25)
plt.title("Random data displayed as two bar plots")

#plt.savefig("DemoPylab__figure11.svg")
#plt.savefig("Demo_Pylab__Figure_11__Bar_plot.png", dpi=120)


# %% Test 12 (Contour Plots)
plt.figure(12)

# A certain function for which we want to see its contours
f = lambda x, y: (1-x/2+x**5+y**3)*np.exp(-x**2-y**2)

n = 512
k = 8
# plt.axis('equal')

x = np.linspace(-3, 3, n)
y = np.linspace(-3, 3, n)
X, Y = np.meshgrid(x, y)  # pylint: disable=W0632
# This meshgrid creates 2 N,N-sized arrays X, Y from two N-size arrays x, y

plt.title(r"2D contour plot based on a function $f(x, y) = (1 - x/2 + x^5 + y^3) \exp(- x^2 - y^2)$")

# Draw the contour
plt.contourf(X, Y, f(X, Y), k, alpha=.75, cmap='hot')

# Write some values
C = plt.contour(X, Y, f(X, Y), k, colors='black', linewidth=0.5)
plt.clabel(C, inline=1, fontsize=18)

# No ticks on the x and y axes
plt.xticks([])
plt.yticks([])

#plt.savefig("DemoPylab__figure12.svg")
#plt.savefig("Demo_Pylab__Figure_12__Contour_plot_3D_surface.png", dpi=120)


# %% Test 13 (One more 3D plot)
fig = plt.figure(13)
from mpl_toolkits.mplot3d import Axes3D  # pylint: disable=W0404
ax = Axes3D(fig)

k = 4
n = 64

X = np.linspace(-k, k, n)
Y = np.linspace(-k, k, n)
X, Y = np.meshgrid(X, Y)  # pylint: disable=W0632
# This meshgrid creates 2 N,N-sized arrays X, Y from two N-size arrays x, y

# Radius, and Z is sin of this radius
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)

plt.title(r"3D surface ($z = \sin(\sqrt{x^2 + y^2})$ and its projection on $xOy$ plane")

ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='hot')

# We also draw the contour, on the (x, y, -2) horizontal plan, but with another color map (cmap)
ax.contourf(X, Y, Z, zdir='z', offset=-2, cmap='winter')

ax.set_zlim(-2, 2)

#plt.savefig("DemoPylab__figure_13.svg")
#plt.savefig("Demo_Pylab__Figure_13__3D_surface_and_projection.png", dpi=120)


# %%
print "Examples are done. More at https://github.com/rougier/matplotlib-tutorial"
