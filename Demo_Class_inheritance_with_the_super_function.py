#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Demo of class inheritance with the super() function from Python standard library.

Note: this example has been sent by a student (thanks to her!).

References are:

 - https://docs.python.org/2/library/functions.html?highlight=super%20function#super
 - http://stackoverflow.com/questions/576169/understanding-python-super-with-init-methods, http://stackoverflow.com/questions/15131282/about-the-super-function
 - http://blog.timvalenta.com/2009/02/understanding-pythons-super/
 - http://code.activestate.com/recipes/577721-how-to-use-super-effectively-python-27-version/


@date: Mon Apr 13 22:18:27 2015.
@author: Lilian Besson for CS101 course at Mahindra Ecole Centrale 2015.
@licence: MIT Licence (http://lbesson.mit-license.org).
"""

# %% Two classes: Rocket and Shuttle

class Rocket(object):
    """ Rocket simulates a rocket ship for a game, or a physics simulation.

    Rocket is inherited from the 'object' class, so it is considered as new type class.
    Reference for that is https://docs.python.org/2/glossary.html#term-new-style-class.
    """

    def __init__(self, x=0, y=0):
        """ A rocket has an (x,y) position, accessible with self.x and self.y. """
        self.x = x
        self.y = y
        
    def move_rocket(self, x_increment=0, y_increment=1):
        """ Move the rocket according to the paremeters given.

        Note: default behavior is to move the rocket up one unit. """
        self.x += x_increment
        self.y += y_increment
     
     
class Shuttle(Rocket):
    """ Shuttle simulates a space shuttle, which is really just a reusable rocket. """
    
    def __init__(self, x=0, y=0, flights_completed=0):
        """ Create a new shuttle object, and uses the special function super. """
        super(Shuttle, self).__init__(x, y)

        # Note that here for this example, the only use of super
        # is to be able to not explicetely name the mother class:
        # « it's easier to read and to maintain » as the doc says.
        # Rocket.__init__(self, x, y)

        assert flights_completed >= 0
        self.flights_completed = flights_completed

    def one_more_flight(self):
        """ One more flight for our space shuttle! """
        self.flights_completed += 1


# %% Here we test our class:
if __name__ == '__main__':
    entreprise = Shuttle(10, 0, 3)
    # A small joke: https://en.wikipedia.org/wiki/Space_Shuttle_Enterprise
    print "We created an object of type:", type(entreprise)
    print "This instance of the Shuttle class is currently located at x =", entreprise.x, "and y =", entreprise.y
    print "And it has done", entreprise.flights_completed, "different travels!"
    entreprise.one_more_flight()
    print "It just went to another travel! Now it has done", entreprise.flights_completed, "different travels!!"


